/**
* Класс, объекты которого описывают параметры гамбургера. 
* 
* @constructor
* @param size        Размер
* @param stuffing    Начинка
* @throws {HamburgerException}  При неправильном использовании
*/
function Hamburger(size, stuffing) {
	try{
		if(arguments.length < 2){
			throw new HamburgerException('Missing required data!');
		}
		if(!Hamburger.sizes[size]){
			throw new HamburgerException(`Invalid size ${size}`);
		}	
		if(!Hamburger.stuffings[stuffing]){
			throw new HamburgerException(`Invalid stuffing ${stuffing}`);
		}
		this.stuffing = stuffing;
		this.size = size;
	}
	catch(e){
	
	}
	this.toppingArray = [];
} 

Hamburger.SIZE_SMALL = 'SIZE_SMALL';
Hamburger.SIZE_LARGE = 'SIZE_LARGE';
Hamburger.sizes = {
	[Hamburger.SIZE_LARGE]: {
		price: 100,
		calories: 40
	},
	[Hamburger.SIZE_SMALL]: {
		price: 50,
		calories: 20
	}
}

Hamburger.STUFFING_CHEESE = 'STUFFING_CHEESE';
Hamburger.STUFFING_SALAD = 'STUFFING_SALAD';
Hamburger.STUFFING_POTATO = 'STUFFING_POTATO';
Hamburger.stuffings = {
	[Hamburger.STUFFING_CHEESE]: {
		price: 10,
		calories: 20
	},
	[Hamburger.STUFFING_SALAD]: {
		price: 20,
		calories: 5
	},
	[Hamburger.STUFFING_POTATO]: {
		price: 15,
		calories: 10
	}
}

Hamburger.TOPPING_MAYO = 'TOPPING_MAYO';
Hamburger.TOPPING_SPICE = 'TOPPING_SPICE';
Hamburger.toppings = {
	[Hamburger.TOPPING_MAYO]: {
		price: 20,
		calories: 5
	},
	[Hamburger.TOPPING_SPICE]: {
		price: 15,
		calories: 0
	}
}

/**
* Добавить добавку к гамбургеру. Можно добавить несколько
* добавок, при условии, что они разные.
* 
* @param topping     Тип добавки
* @throws {HamburgerException}  При неправильном использовании
*/
Hamburger.prototype.addTopping = function (topping){
	let isPresent = this.toppingArray.some(function(elem){
		return elem === topping;
		});
	try{
		if(!isPresent){
			this.toppingArray.push(topping);
		}
		else{
			throw new HamburgerException(`Duplicate topping: ${topping}`);
		}
	}
	catch(e){
		
	}
}

/**
 * Убрать добавку, при условии, что она ранее была 
 * добавлена.
 * 
 * @param topping   Тип добавки
 * @throws {HamburgerException}  При неправильном использовании
 */
Hamburger.prototype.removeTopping = function (topping){
	let toppingIndex = this.toppingArray.indexOf(topping);
	try{
		if(~toppingIndex){
			this.toppingArray.splice(toppingIndex, 1);
		}
		else{
			throw new HamburgerException(`You do not have this topping: ${topping}!`);
		}
	}
	catch(e){
		
	}
}

/**
 * Получить список добавок.
 *
 * @return {Array} Массив добавленных добавок, содержит константы
 *                 Hamburger.TOPPING_*
 */
Hamburger.prototype.getToppings = function (){
	return this.toppingArray;
}

/**
 * Узнать размер гамбургера
 */
Hamburger.prototype.getSize = function (){
	return this.size;
}

/**
 * Узнать начинку гамбургера
 */
Hamburger.prototype.getStuffing = function (){
	return this.stuffing;
}

/**
 * Узнать цену гамбургера
 * @return {Number} Цена в тугриках
 */
Hamburger.prototype.calculatePrice = function (){
	return Hamburger.sizes[this.size].price + Hamburger.stuffings[this.stuffing].price + this.toppingArray.reduce(function(accum, current){
		return accum + Hamburger.toppings[current].price;
	}, 0);
}

/**
 * Узнать калорийность
 * @return {Number} Калорийность в калориях
 */
Hamburger.prototype.calculateCalories = function (){
	return Hamburger.sizes[this.size].calories + Hamburger.stuffings[this.stuffing].calories + this.toppingArray.reduce(function(accum, current){
		return accum + Hamburger.toppings[current].calories;
	}, 0);
}

/**
 * Представляет информацию об ошибке в ходе работы с гамбургером. 
 * Подробности хранятся в свойстве message.
 * @constructor 
 */
function HamburgerException(message){
	this.message = message;
	console.log(message);
}

/* for tests
const ham = new Hamburger();
const h = new Hamburger(Hamburger.SIZE_LARGE, Hamburger.STUFFING_CHEESE);
h.addTopping(Hamburger.TOPPING_MAYO);
h.addTopping(Hamburger.TOPPING_SPICE);
h.addTopping(Hamburger.TOPPING_SPICE);
h.removeTopping(Hamburger.TOPPING_SPICE);
h.removeTopping(Hamburger.TOPPING_SPICE);
h.addTopping(Hamburger.TOPPING_MAYO);
console.log(h.calculateCalories());
console.log(h.calculatePrice());
console.log(h.getToppings());
console.log(h.getSize() === Hamburger.SIZE_SMALL);
console.log(h.getStuffing());
*/